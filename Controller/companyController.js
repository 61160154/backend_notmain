const Company = require('../models/Company')
const companyService = {
  async getAllCompany (req, res, next) {
    try {
      const allCompany = await Company.find({})
      res.json(allCompany)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompany (req, res, next) {
    try {
      const company = await Company.findById({
        _id: '603cdb546ae2153820a91a1b'
      })
      console.log(company)
      res.status(200).json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addCompany (req, res, next) {
    const payload = req.body
    console.log(payload)
    const company = new Company(payload)
    console.log(company)
    try {
      const companyNew = await company.save()
      res.json(companyNew)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCompany (req, res, next) {
    const payload = req.body
    try {
      const company = await Company.updateOne({ _id: payload._id }, payload)
      console.log(company)
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompanyId (req, res, next) {
    try {
      const { id } = req.params
      const company = await Company.findById({ _id: id })
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = companyService
