const detailDeclare = require('../models/DetailDeclare')

const detail = {
  async getDeclareList (req, res, next) {
    try {
      const detail = await detailDeclare.find({})
      res.json(detail)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDeclareId (req, res, next) {
    try {
      const { id } = req.params
      const detail = await detailDeclare.findById(id)
      res.json(detail)
      console.log(detail)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = detail
