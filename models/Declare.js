// const { Int32 } = require('mongodb')
const mongoose = require('mongoose')
const declareSchema = new mongoose.Schema({
  id: Number,
  dateStart: String,
  dateClose: String,
  statusDeclare: String,
  position: {
    positionName: String,
    workType: String,
    gender: String,
    age: String,
    rate: Number,
    salary: Number,
    detail: String,
    property: [Array],
    note: ''
  }
})

module.exports = mongoose.model('Declare', declareSchema)
