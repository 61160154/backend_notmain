const express = require('express')
const router = express.Router()
const declareController = require('../controller/declarListController')

router.get('/', (req, res) => {
  res.json(declareController.getDeclareList())
})

router.get('/', declareController.getDeclareList)

router.get('/:id', declareController.getDeclareId)

router.put('/', declareController.updateDeclare)

module.exports = router
