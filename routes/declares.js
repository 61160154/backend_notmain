const express = require('express')
// const { getRecentDeclare } = require('../controller/declarListController')
const router = express.Router()
const declareController = require('../controller/declarListController')

// Declare
// get declare ทั้งหมด
router.get('/', declareController.getDeclareList)

// get declare ทีละไอดี
router.get('/:id', declareController.getDeclareId)

// add declare ใหม่เข้าไป
router.post('/', declareController.addDeclare)

// update declare เข้าไปใหม่
router.put('/', declareController.updateDeclare)

// delete declare ด้วย id
router.delete('/:id', declareController.deleteDeclare)

// get declare ล่าสุด 4 อันของ currentCompany
router.get('/recent/:id', declareController.getLastDeclares)

module.exports = router
